const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

let header = document.createElement("div");
header.textContent = "Kata 0";
document.body.appendChild(header);
function kata0() {
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement)
    return lotrCitiesArray;
}
kata0();

let header1 = document.createElement("div");
header1.textContent = "Kata 1";
document.body.appendChild(header1);
function kata1(){
    let newElement = document.createElement('div');
    newElement.textContent = JSON.stringify(gotCitiesCSV);
    document.body.appendChild(newElement)
    return gotCitiesCSV 
    
}
kata1();

let header2 = document.createElement("div");
header2.textContent = "Kata 2";
document.body.appendChild(header2);
function kata2(){
    let words = bestThing.split(" ")
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(words);
    document.body.appendChild(newElement)
    

    return words
}
kata2();

let header3 = document.createElement("div");
header3.textContent = "Kata 3";
document.body.appendChild(header3);
function kata3(){
    let symbol = gotCitiesCSV.replace(/,/g, ':')
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(symbol);
    document.body.appendChild(newElement);
    console.log(gotCitiesCSV);
    return symbol
}
kata3();

let header4 = document.createElement("div");
header4.textContent = "Kata 4";
document.body.appendChild(header4);
function kata4(){
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lotrCitiesArray);
    document.body.appendChild(newElement);
    return lotrCitiesArray

}
kata4();

let header5 = document.createElement("div");
header5.textContent = "Kata 5";
document.body.appendChild(header5);
function kata5(){
    let first5 = lotrCitiesArray.slice(0,5);
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(first5);
    document.body.appendChild(newElement);
    
    return first5
}
kata5();

let header6 = document.createElement("div");
header6.textContent = "Kata 6";
document.body.appendChild(header6);
function kata6(){
    let last5 = lotrCitiesArray.slice(3,8);
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(last5);
    document.body.appendChild(newElement);
    
    return last5
}
kata6();

let header7 = document.createElement("div");
header7.textContent = "Kata 7";
document.body.appendChild(header7);
function kata7(){
    let thirdtoFifth = lotrCitiesArray.slice(2,5);
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(thirdtoFifth);
    document.body.appendChild(newElement);
    
    return thirdtoFifth
}
kata7();
let header8 = document.createElement("div");
header8.textContent = "Kata 8";
document.body.appendChild(header8);
function kata8(){
    let cloneArray = lotrCitiesArray;
    lotrCitiesArray.splice(2,1);
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(cloneArray);
    document.body.appendChild(newElement);
    return cloneArray
    
}
kata8();
let header9 = document.createElement("div");
header9.textContent = "Kata 9";
document.body.appendChild(header9);
function kata9(){
    let cloneArray = lotrCitiesArray
    lotrCitiesArray.splice(5,2)
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(cloneArray);
    document.body.appendChild(newElement);
    return cloneArray
}
kata9();

let header10 = document.createElement("div");
header10.textContent = "Kata 10";
document.body.appendChild(header10);
function kata10(){
    let reWord = lotrCitiesArray
    lotrCitiesArray.splice(1,0,'Rohan')
    lotrCitiesArray.join("Rohan")
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(reWord);
    document.body.appendChild(newElement);
    return reWord
}
kata10();

let header11 = document.createElement("div");
header11.textContent = "Kata 11";
document.body.appendChild(header11);
function kata11(){
    let change = lotrCitiesArray
    lotrCitiesArray.splice(5,1,"Deadest Marshes")
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(change);
    document.body.appendChild(newElement);
    return change
}
kata11();

let header12 = document.createElement("div");
header12.textContent = "Kata 12";
document.body.appendChild(header12);
function kata12(){
    let nSent = bestThing.slice(0,15)
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(nSent);
    document.body.appendChild(newElement);
    return nSent
}
kata12();

let header13 = document.createElement("div");
header13.textContent = "Kata 13";
document.body.appendChild(header13);
function kata13(){
    let lastSent = bestThing.slice(68)
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lastSent);
    document.body.appendChild(newElement);
    return lastSent
}
kata13();

let header14 = document.createElement("div");
header14.textContent = "Kata 14";
document.body.appendChild(header14);
function kata14(){
    let words = bestThing.slice(23,38)
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(words);
    document.body.appendChild(newElement);
    return words
}
kata14();

let header15 = document.createElement("div");
header15.textContent = "Kata 15";
document.body.appendChild(header15);
function kata15(){
    let lastSent2 = bestThing.substr(68)
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(lastSent2);
    document.body.appendChild(newElement);
    return lastSent2
}
kata15();

let header16 = document.createElement("div");
header16.textContent = "Kata 16";
document.body.appendChild(header16);
function kata16(){
    let Lwords = bestThing.substr(23,15)
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(Lwords);
    document.body.appendChild(newElement);
    return Lwords
}
kata16();

let header17 = document.createElement("div");
header17.textContent = "Kata 17";
document.body.appendChild(header17);
function kata17(){
    let popwords = lotrCitiesArray
    lotrCitiesArray.pop()
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(popwords);
    document.body.appendChild(newElement);
    return popwords
}
kata17();

let header18 = document.createElement("div");
header18.textContent = "Kata 18";
document.body.appendChild(header18);
function kata18(){
    let pushwords = lotrCitiesArray
    lotrCitiesArray.push("Deadest Marshes")
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(pushwords);
    document.body.appendChild(newElement);
    return pushwords
}
kata18();

let header19 = document.createElement("div");
header19.textContent = "Kata 19";
document.body.appendChild(header19);
function kata19(){
    let shiftwords = lotrCitiesArray
    lotrCitiesArray.shift()
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(shiftwords);
    document.body.appendChild(newElement);
    return shiftwords
}
kata19();

let header20 = document.createElement("div");
header20.textContent = "Kata 20";
document.body.appendChild(header20);
function kata20(){
    let unwords = lotrCitiesArray
    lotrCitiesArray.unshift("Mordor")
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(unwords);
    document.body.appendChild(newElement);
    return unwords
}
kata20();


















